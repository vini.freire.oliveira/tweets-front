$(document).ready(function(){

  var settings_relevants = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:3000/most_relevants",
    "method": "GET"
  }
  
  $.ajax(settings_relevants).done(function (response) {
    create_most_relevants_list(response);
  });

  var settings_mentions = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:3000/most_mentions",
    "method": "GET"
  }
  
  $.ajax(settings_mentions).done(function (response) {
    create_most_mentions_list(response)
  });

})



function create_most_relevants_list(data){
  var list_html =  document.getElementById("relevants_list");
  list_html.innerHTML = "";

  for(var i=0; i<data.length;i++){
    var content = `<div class="col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="images/twitter.png" alt="card image"></p>
                                        <h4 class="card-title">`+data[i].screen_name+`</h4>
                                        <p class="card-text"> `+data[i].followers_count+` followers</p>
                                        <p class="card-text">`+data[i].created_at+`</p>
                                        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">`+data[i].screen_name+`</h4>
                                        <p class="card-text">`+data[i].text+`</p>
                                        <p class="card-text">`+data[i].favorite_count+` favorites</p>
                                        <p class="card-text">`+data[i].retweet_count+` retweets</p>
                                        <p class="card-text">`+data[i].link+`</p>
                                        <a class="social-icon text-xs-center" target="_blank" href="`+data[i].profile_link+`">Profile Link</a>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="https://www.linkedin.com/in/vinicius-freire-b53507107/">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="https://gitlab.com/vini.freire.oliveira">
                                                    <i class="fa fa-git"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;

    list_html.innerHTML += content;
  }
}



function create_most_mentions_list(data){
  var list_html =  document.getElementById("mentions_list");
  list_html.innerHTML = "";

  for(var i=0; i<data.length;i++){
    var name = Object.keys(data[i]);
    var values = Object.values(data[i])[0];

    var content = `<div class="col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="images/twitter.png" alt="card image"></p>
                                        <h4 class="card-title">`+name+`</h4>
                                        <p class="card-text">`+values.text+`</p>
                                        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">`+values.screen_name+`</h4>
                                        <p class="card-text">`+values.followers_count+` followers</p>
                                        <p class="card-text">`+values.favorite_count+` favorites</p>
                                        <p class="card-text">`+values.retweet_count+` retweets</p>
                                        <p class="card-text">`+values.link+`</p>
                                        <a class="social-icon text-xs-center" target="_blank" href="`+values.profile_link+`">Profile Link</a>
                                        <p class="card-text">`+values.created_at+`</p>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="https://www.linkedin.com/in/vinicius-freire-b53507107/">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="https://gitlab.com/vini.freire.oliveira">
                                                    <i class="fa fa-git"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;

    list_html.innerHTML += content;
  }
}